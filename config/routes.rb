Rails.application.routes.draw do
  resources :companies
  root 'pages#home'
end
